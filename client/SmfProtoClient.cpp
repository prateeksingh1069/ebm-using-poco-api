
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "SmfProtoClient.h"
#include "../proto/smf.pb.h"
#include "../proto/manet.pb.h"
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"


using Poco::Net::DatagramSocket;
using Poco::Net::SocketAddress;
using Poco::UInt16;

SmfProtoClient::SmfProtoClient() {
	// TODO Auto-generated constructor stub

}

SmfProtoClient::~SmfProtoClient() {
	// TODO Auto-generated destructor stub
	DatagramSocket socket;
	socket.close();
}

void SmfProtoClient::SendToSmf(std::string host, bool isRel) //For E-CDS
{
	static const int SMF_PORT = 5557;

	SocketAddress address(host, SMF_PORT);
	SocketAddress sa;

	DatagramSocket socket;

	Manet::RouterMessage router;
	    router.set_id("");

	    if(isRel)
	        router.SetExtension(Manet::SmfRouterMessage::isRelay,true);
	    else
	        router.SetExtension(Manet::SmfRouterMessage::isRelay,false);


	    Manet::SmfMessage request;
	    Manet::SmfMessageHeader *header = request.mutable_header();
	    header->set_messagetype(Manet::ROUTER_RELAY_ECDS);  // For ECDS case in smfDispatcher
	    header->set_optype(Manet::POST);
	    request.set_message(router.SerializeAsString());

	    if(socket.sendTo(request.SerializeAsString().c_str(), request.ByteSize(), address))
	    {
	        std::cout << "successful send" << std::endl;
			char buf[8192];
	        unsigned int size = 8192;
	        if(socket.receiveFrom(buf,size,sa))
	        {
				std::cout << "Received. " << std::endl;
				Manet::SmfMessage response;
	            response.ParseFromArray(buf,size);
				std::cout << "Response Code:	" << Manet::ManetResponseCode_Name(response.header().responsecode()) << std::endl;
				if ( response.header().responsecode() == Manet::SUCCESSFUL )
				{
					Manet::RouterMessage routerResponse;
					routerResponse.ParseFromString(response.message());
					std::cout << "Is relay selected(1 or 0)? " << routerResponse.GetExtension(Manet::SmfRouterMessage::isRelay) << std::endl;
				}
				else
					std::cout << response.message() << std::endl;
			}
	    }
	    else
	    {
	        std::cout << "send to socket failed" << std::endl;
	    }

	socket.close();

}


void SmfProtoClient::SendToSmf(std::string host, const char* MPR_selector_set)  //For S-MPR
{
	static const int SMF_PORT = 5557;

	SocketAddress address(host, SMF_PORT);
	SocketAddress sa;

	DatagramSocket socket;

	Manet::RouterMessage router;
	    router.set_id("");

	    //if(isRel)
	        //router.SetExtension(Manet::SmfRouterMessage::MPR,MPR_selector_set);
	    //else
	        //router.SetExtension(Manet::SmfRouterMessage::isRelay,false);

	    router.set_mpr(MPR_selector_set);


	    Manet::SmfMessage request;
	    Manet::SmfMessageHeader *header = request.mutable_header();
	    header->set_messagetype(Manet::ROUTER_RELAY_SMPR);  // For SMPR case in smfDispatcher
	    header->set_optype(Manet::POST);
	    request.set_message(router.SerializeAsString());

	    if(socket.sendTo(request.SerializeAsString().c_str(), request.ByteSize(), address))
	    {
	        std::cout << "successful send" << std::endl;
			char buf2[8192];
	        unsigned int size = 8192;
	        if(socket.receiveFrom(buf,size,sa))
	        {
				std::cout << "Received. " << std::endl;
				Manet::SmfMessage response;
	            response.ParseFromArray(buf,size);
				std::cout << "Response Code:	" << Manet::ManetResponseCode_Name(response.header().responsecode()) << std::endl;
				if ( response.header().responsecode() == Manet::SUCCESSFUL )
				{
					Manet::RouterMessage routerResponse;
					routerResponse.ParseFromString(response.message());
					std::cout << "Is relay selected(1 or 0)? " << routerResponse.GetExtension(Manet::SmfRouterMessage::isRelay) << std::endl;
				}
				else
					std::cout << response.message() << std::endl;
			}
	    }
	    else
	    {
	        std::cout << "send to socket failed" << std::endl;
	    }

	socket.close();

}


void SmfProtoClient::SendToSmf_NgbrList(std::string host, const char* Neighbour_selector_set)  //For S-MPR
{
	static const int SMF_PORT = 5557;

	SocketAddress address(host, SMF_PORT);
	SocketAddress sa;

	DatagramSocket socket;

	Manet::RouterMessage router;
	    router.set_id("");

	    //if(isRel)
	        //router.SetExtension(Manet::SmfRouterMessage::MPR,MPR_selector_set);
	    //else
	        //router.SetExtension(Manet::SmfRouterMessage::isRelay,false);

	    router.set_mpr(Neighbour_selector_set);


	    Manet::SmfMessage request;
	    Manet::SmfMessageHeader *header = request.mutable_header();
	    header->set_messagetype(Manet::ROUTER_RELAY_SMPR_NGH);  // For SMPR case in smfDispatcher
	    header->set_optype(Manet::POST);
	    request.set_message(router.SerializeAsString());

	    if(socket.sendTo(request.SerializeAsString().c_str(), request.ByteSize(), address))
	    {
	        std::cout << "successful send" << std::endl;
			char buf4[8192];
	        unsigned int size = 8192;
	        if(socket.receiveFrom(buf4,size,sa))
	        {
				std::cout << "Received. " << std::endl;
				Manet::SmfMessage response;
	            response.ParseFromArray(buf,size);
				std::cout << "Response Code:	" << Manet::ManetResponseCode_Name(response.header().responsecode()) << std::endl;
				if ( response.header().responsecode() == Manet::SUCCESSFUL )
				{
					Manet::RouterMessage routerResponse;
					routerResponse.ParseFromString(response.message());
					std::cout << "Is relay selected(1 or 0)? " << routerResponse.GetExtension(Manet::SmfRouterMessage::isRelay) << std::endl;
				}
				else
					std::cout << response.message() << std::endl;
			}
	    }
	    else
	    {
	        std::cout << "send to socket failed" << std::endl;
	    }

	socket.close();

}


void SmfProtoClient::SendToSmfN_SMPR(std::string host, const char* MPR_selector_set) //For NS-MPR
{
	static const int SMF_PORT = 5557;

	SocketAddress address(host, SMF_PORT);
	SocketAddress sa;

	DatagramSocket socket;

	Manet::RouterMessage router;
	    router.set_id("");

	    router.set_mpr(MPR_selector_set);


	    Manet::SmfMessage request;
	    Manet::SmfMessageHeader *header = request.mutable_header();
	    header->set_messagetype(Manet::ROUTER_RELAY_NSMPR);  // For NS-MPR case in smfDispatcher
	    header->set_optype(Manet::POST);
	    request.set_message(router.SerializeAsString());

	    if(socket.sendTo(request.SerializeAsString().c_str(), request.ByteSize(), address))
	    {
	        std::cout << "successful send" << std::endl;
			char buf3[8192];
	        unsigned int size = 8192;
	        if(socket.receiveFrom(buf,size,sa))
	        {
				std::cout << "Received. " << std::endl;
				Manet::SmfMessage response;
	            response.ParseFromArray(buf,size);
				std::cout << "Response Code:	" << Manet::ManetResponseCode_Name(response.header().responsecode()) << std::endl;
				if ( response.header().responsecode() == Manet::SUCCESSFUL )
				{
					Manet::RouterMessage routerResponse;
					routerResponse.ParseFromString(response.message());
					std::cout << "Is relay selected(1 or 0)? " << routerResponse.GetExtension(Manet::SmfRouterMessage::isRelay) << std::endl;
				}
				else
					std::cout << response.message() << std::endl;
			}
	    }
	    else
	    {
	        std::cout << "send to socket failed" << std::endl;
	    }

	socket.close();

}


