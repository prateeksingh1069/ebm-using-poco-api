//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfUtilities.h"
#include "protoAddress.h"
#include "protoDebug.h"


SmfUtil* SmfUtil::_instance = NULL;

SmfUtil* SmfUtil::GetInstance()
{
    if(_instance == NULL)
    {
        _instance = new SmfUtil;
    }
    return _instance;
}

bool SmfUtil::Pack(Manet::SmfMessage &message, char const *const buffer, unsigned int const length)
{
    try
    {
        if(buffer != NULL)
        {
            std::string str;
            // Set length of string to the length of the incoming char* buffer
            //
            str.resize(length);
            for(unsigned int i = 0; i < length; i++)
            {
                str[i] = buffer[i];
            }

            message.ParseFromString(str);
        }
        else
        {
            return false;
        }
    }
    catch(google::protobuf::exception& e)
    {
        DMSG(6, e.what());
        return false;
    }
    catch(...)
    {
        DMSG(6, "unknown exception");
        return false;
    }

    return true;
}

bool SmfUtil::IsLocalHost(const char* host)
{
    const int HOSTNAME_LENGTH = 256;
    ProtoAddress localAddress;
    char localhost[HOSTNAME_LENGTH];
    strcpy(localhost, host);
    if(localAddress.ResolveLocalAddress(localhost, HOSTNAME_LENGTH))
    {
        return true;
    }
    return false;
}
