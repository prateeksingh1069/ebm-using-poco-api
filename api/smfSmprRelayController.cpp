//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfSmprRelayController.h"
#include "smfService.h"

SmfSmprRelayController::SmfSmprRelayController(SmfService* smfServicePtr, Manet::SmfMessageType type)
    : SmfController(smfServicePtr)
{
    _type = type;
}

void SmfSmprRelayController::HandleGetRequest(Manet::SmfMessage requestMessage)
{

}

void SmfSmprRelayController::HandlePutRequest(Manet::SmfMessage requestMessage)
{

}

void SmfSmprRelayController::HandlePostRequest(Manet::SmfMessage requestMessage)
{
    Manet::SmfMessage responseMessage;
    Manet::SmfMessageHeader *responseMessageHeader = responseMessage.mutable_header();

    if( requestMessage.has_message() )
    {
        //DMSG(0, "Inside  requestMessage.has_message()\n");
        Manet::RouterMessage routerRequest;
        routerRequest.ParseFromString(requestMessage.message());

        if(requestMessage.header().messagetype() == Manet::ROUTER_RELAY_SMPR)
        {
            //DMSG(0, "Inside  requestMessage.header().messagetype() == Manet::ROUTER_RELAY_ECDS\n");
            responseMessageHeader->set_messagetype(Manet::ROUTER_RELAY_SMPR);
            Manet::RouterMessage routerResponse;

			bool is_relay;
            if(_smfServicePtr->SetSmprRelayStatus(routerRequest))
            {
                //DMSG(0, "Inside  _smfServicePtr->SetEcdsRelayStatus(routerRequest,is_relay)\n");
				routerResponse.set_id("");
				routerResponse.SetExtension(Manet::SmfRouterMessage::isRelay,is_relay);

                responseMessageHeader->set_responsecode(Manet::SUCCESSFUL);
                responseMessage.set_message(routerResponse.SerializeAsString());
            }
            else
            {
                //DMSG(0, "NOT Inside  _smfServicePtr->SetEcdsRelayStatus(routerRequest,is_relay)\n");
                responseMessageHeader->set_responsecode(Manet::BAD_OPTION);
                responseMessage.set_message("Could not SET Relay Status");
            }
        }
        if(requestMessage.header().messagetype() == Manet::ROUTER_RELAY_SMPR_NGH)
        {
            //DMSG(0, "Inside  requestMessage.header().messagetype() == Manet::ROUTER_RELAY_ECDS\n");
            responseMessageHeader->set_messagetype(Manet::ROUTER_RELAY_SMPR_NGH);
            Manet::RouterMessage routerResponse;

			bool is_relay;
            if(_smfServicePtr->SetSmprNGHStatus(routerRequest))
            {
                //DMSG(0, "Inside  _smfServicePtr->SetSmprNGHStatus(routerRequest)\n");
				routerResponse.set_id("");
				routerResponse.SetExtension(Manet::SmfRouterMessage::isRelay,is_relay);

                responseMessageHeader->set_responsecode(Manet::SUCCESSFUL);
                responseMessage.set_message(routerResponse.SerializeAsString());
            }
            else
            {
                //DMSG(0, "NOT Inside  smfServicePtr->SetSmprNGHStatus(routerRequest))\n");
                responseMessageHeader->set_responsecode(Manet::BAD_OPTION);
                responseMessage.set_message("Could not SET Relay Status");
            }
        }
    }
    else
    {
        //DMSG(0, "NOT Inside  requestMessage.has_message()\n");
        responseMessageHeader->set_responsecode(Manet::BAD_MESSAGE);
        responseMessage.set_message("Could not SET Relay Status");
    }

    _response = responseMessage.SerializeAsString();
    _responseLength = responseMessage.ByteSize();
}

void SmfSmprRelayController::HandleDeleteRequest(Manet::SmfMessage requestMessage)
{

}
