//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "manet.pb.h"
#include "smf.pb.h"
//#include "../../../../Documents/EBM/proto/manet.pb.h"
//#include "../../../../Documents/EBM/proto/smf.pb.h"
#include "smfDispatcher.h"
#include "smf.h"
#include "smfService.h"
#include "smfUtilities.h"
#include "smfRouterController.h"
#include "smfInterfaceController.h"
#include "smfEcdsRelayController.h"
#include "smfSmprRelayController.h"
#include "smfNSmprRelayController.h"

SmfDispatcher::SmfDispatcher(Smf* smfPtr)
    : _smfServicePtr(new SmfService(smfPtr)), _controller(0)
{
}

SmfDispatcher::~SmfDispatcher()
{
    if (_smfServicePtr)
        delete _smfServicePtr;
}

bool SmfDispatcher::Dispatch(char* request, unsigned int const length)
{
    Manet::SmfMessage requestMessage;
    DMSG(0, "Inside  SmfDispatcher::Dispatch\n");
    if(SmfUtil::GetInstance()->Pack(requestMessage, request, length))
    {
        DMSG(0, "Inside  SmfUtil::GetInstance()->Pack(requestMessage, request, length)\n");
        switch (requestMessage.header().messagetype())
        {
            DMSG(0, "Inside  switch (requestMessage.header().messagetype())\n");
            case Manet::ROUTER:
                _controller = new SmfRouterController(_smfServicePtr, requestMessage.header().messagetype());
                break;
            case Manet::INTERFACE:
            case Manet::INTERFACE_SET:
                _controller = new SmfInterfaceController(_smfServicePtr, requestMessage.header().messagetype());
                break;
			case Manet::ROUTER_RELAY_ECDS:
				_controller = new SmfEcdsRelayController(_smfServicePtr, requestMessage.header().messagetype());
                DMSG(0, "Manet::ROUTER_RELAY_ECDS \n");
				break;
			case Manet::ROUTER_RELAY_SMPR:
			    _controller = new SmfSmprRelayController(_smfServicePtr, requestMessage.header().messagetype());
                DMSG(0, "Manet::ROUTER_RELAY_SMPR \n");
				break;
            case Manet::ROUTER_RELAY_SMPR_NGH:
			    _controller = new SmfSmprRelayController(_smfServicePtr, requestMessage.header().messagetype());
                DMSG(0, "Manet::ROUTER_RELAY_SMPR \n");
				break;
			case Manet::ROUTER_RELAY_NSMPR:
			    _controller = new SmfNSmprRelayController(_smfServicePtr, requestMessage.header().messagetype());
                DMSG(0, "Manet::ROUTER_RELAY_NSMPR \n");
				break;            
            case Manet::NEIGHBOR:
            case Manet::NEIGHBOR_SET:
            case Manet::MULTICAST_GROUP:
            case Manet::MULTICAST_GROUP_SET:						
            default:
                break;
        }
    }

    if (NULL != _controller)
    {
        _controller->ProcessRequest(requestMessage);
        return true;
    }
    else
    {
        DMSG(0, "NULL = _controller \n");
    }

    return false;
}

void SmfDispatcher::SendResponse(ProtoAddress::Type addressType, const char* host, const int port)
{
    ProtoAddress destAddress;
    destAddress.ResolveFromString(host);
    destAddress.SetPort(port);

    DMSG(0, "[SmfDispatcher::SendResponse] sending response to host [%s] port [%d]\n", host, destAddress.GetPort());

    std::string const &responseMessage = _controller->GetMessage();
    ProtoSocket sender(Smf::DEFAULT_API_PROTOCOL);
    //sender.Open(0, addressType, false);
    sender.SendTo(responseMessage.c_str(), responseMessage.length(), destAddress);
}
