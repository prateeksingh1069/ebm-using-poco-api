//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfService.h"
#include "smf.h"
#include <stdlib.h>  // for atoi()
#include <stdio.h>   // for stdout/stderr printouts
#include <string.h>
#include <ctype.h>  // for "isspace()"

SmfService::SmfService(Smf* smfPtr)
    :  _smfPtr(smfPtr)
{
}

SmfService::~SmfService()
{
}

bool SmfService::GetRouterItem(Manet::RouterMessage &routerMessage)
{
    if(routerMessage.HasExtension(Manet::SmfRouterMessage::inPkts))
        routerMessage.SetExtension(Manet::SmfRouterMessage::inPkts, _smfPtr->GetMulticastReceiveCount());
    if(routerMessage.HasExtension(Manet::SmfRouterMessage::outPkts))
        routerMessage.SetExtension(Manet::SmfRouterMessage::outPkts, _smfPtr->GetForwardedCount());
    if(routerMessage.HasExtension(Manet::SmfRouterMessage::duplicatedPkts))
        routerMessage.SetExtension(Manet::SmfRouterMessage::duplicatedPkts, _smfPtr->GetDupCount());

    return true;
}

bool SmfService::GetInterfaceItem(Manet::InterfaceMessage &interfaceMessage)
{

    return false;
}

bool SmfService::SetRouterItem(Manet::RouterMessage const &routerRequest,
		Manet::RouterMessage &routerResponse)
{
    Smf::InterfaceList::Iterator iterator(_smfPtr->AccessInterfaceList());
    Smf::Interface* iface;
    while (NULL != (iface = iterator.GetNextItem()))
    {
        Smf::Interface::AssociateIterator iterator(*iface);
        Smf::Interface::Associate* assoc;
        while (NULL != (assoc = iterator.GetNextAssociate()))
        {
            switch(routerRequest.GetExtension(Manet::SmfRouterMessage::rssaType))
            {
                case Manet::CF:
                    assoc->SetRelayType(Smf::CF);
                    break;
                case Manet::ECDS:
                    assoc->SetRelayType(Smf::E_CDS);
                    break;
                default:
                    break;
            }
        }
    }

	return true;
}

bool SmfService::SetEcdsRelayStatus(Manet::RouterMessage const &routerRequest, bool &is_relay)
{
    if (routerRequest.GetExtension(Manet::SmfRouterMessage::isRelay))
		_smfPtr->SetRelaySelected(true);
	else
		_smfPtr->SetRelaySelected(false);
	
	//test
	is_relay = _smfPtr->GetRelaySelected();
	
	return true;
}

bool SmfService::SetSmprRelayStatus(Manet::RouterMessage const &routerRequest)
{
    if (routerRequest.has_mpr())
    {
        const char* mpr_relay_sel = routerRequest.mpr().c_str();
        unsigned int mprLen = strlen(mpr_relay_sel);
		_smfPtr->SetSelectorList(mpr_relay_sel, mprLen);
        DMSG(0, "routerRequest.has_mpr() = true. mpr_relay_sel = %s \n", mpr_relay_sel);     
    }
	else
		DMSG(0, "routerRequest.has_mpr() = false. No data!!!!\n");
        
	
	//test
	is_relay = _smfPtr->GetRelaySelected();
	
	return true;
}

bool SmfService::SetSmprNGHStatus(Manet::RouterMessage const &routerRequest)
{
    if (routerRequest.has_mpr())
    {
        const char* mpr_ngh_sel = routerRequest.mpr().c_str();
        unsigned int mprLen = strlen(mpr_ngh_sel);
		_smfPtr->SetNeighborList(mpr_ngh_sel, mprLen);
        DMSG(0, "routerRequest.has_mpr() = true. mpr_ngh_sel = %s \n", mpr_ngh_sel);     
    }
	else
		DMSG(0, "routerRequest.has_mpr() = false. No data!!!!\n");
        
	
	//test
	is_relay = _smfPtr->GetRelaySelected();
	
	return true;
}

bool SmfService::SetNSmprRelayStatus(Manet::RouterMessage const &routerRequest)
{
    const char* mpr_relay_sel = routerRequest.mpr().c_str();
    unsigned int mprLen = strlen(mpr_relay_sel);
    DMSG(0, "mpr_relay_sel = %s, mprLen = %d , routerRequest.has_mpr() = %d, (mpr_relay_sel != NULL) = %d \n", mpr_relay_sel, mprLen, routerRequest.has_mpr(), (mpr_relay_sel != NULL));
    if (mprLen != 0)
    {
        _smfPtr->SetRelaySelected(true);
        DMSG(0, "routerRequest.has_mpr() = true. _smfPtr->SetRelaySelected(true) \n");     
    }
	else
    {
        _smfPtr->SetRelaySelected(false);
		DMSG(0, "routerRequest.has_mpr() = false. _smfPtr->SetRelaySelected(false) \n");
    }        
	
	//test
	is_relay = _smfPtr->GetRelaySelected();
	
	return true;
}
