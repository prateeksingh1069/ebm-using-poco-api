//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfController.h"
#include "smfService.h"

SmfController::SmfController(SmfService* smfServicePtr)
    : _smfServicePtr(smfServicePtr), _response(""), _responseLength(0)
{
}

void SmfController::ProcessRequest(Manet::SmfMessage requestMessage)
{
	if (requestMessage.header().messagetype() != _type)
		return;

	switch(requestMessage.header().optype())
	{
		case Manet::GET:
			HandleGetRequest(requestMessage);
			break;
		//case Manet::GET_NEXT:
		  //  HandleGetNextRequest(requestMessage);
		case Manet::PUT:
			HandlePutRequest(requestMessage);
			break;
		case Manet::POST:
			HandlePostRequest(requestMessage);
			break;
		case Manet::DELETE:
			HandleDeleteRequest(requestMessage);
			break;
		default:
			break;
	}

}
