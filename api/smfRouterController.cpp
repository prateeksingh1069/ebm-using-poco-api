//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfRouterController.h"
#include "smfService.h"

SmfRouterController::SmfRouterController(SmfService* smfServicePtr, Manet::SmfMessageType type)
    : SmfController(smfServicePtr)
{
    _type = type;
}

void SmfRouterController::HandleGetRequest(Manet::SmfMessage requestMessage)
{
    Manet::SmfMessage responseMessage;
    Manet::SmfMessageHeader *responseMessageHeader = responseMessage.mutable_header();

    if( requestMessage.has_message() )
    {
        Manet::RouterMessage routerMessage;
        routerMessage.ParseFromString(requestMessage.message());

        if(requestMessage.header().messagetype() == Manet::ROUTER)
        {
            responseMessageHeader->set_messagetype(Manet::ROUTER);

            if(_smfServicePtr->GetRouterItem(routerMessage))
            {
                responseMessageHeader->set_responsecode(Manet::SUCCESSFUL);
                responseMessage.set_message(routerMessage.SerializeAsString());
            }
            else
            {
                responseMessageHeader->set_responsecode(Manet::BAD_OPTION);
                responseMessage.set_message("Could not GET Router Item");
            }
        }
    }
    else
    {
        responseMessageHeader->set_responsecode(Manet::BAD_MESSAGE);
        responseMessage.set_message("Could not SET Router Item");
    }

    _response = responseMessage.SerializeAsString();
    _responseLength = responseMessage.ByteSize();
}

void SmfRouterController::HandleGetNextRequest(Manet::SmfMessage requestMessage)
{

}

void SmfRouterController::HandlePutRequest(Manet::SmfMessage requestMessage)
{

}

void SmfRouterController::HandlePostRequest(Manet::SmfMessage requestMessage)
{
    Manet::SmfMessage responseMessage;
    Manet::SmfMessageHeader *responseMessageHeader = responseMessage.mutable_header();

    if( requestMessage.has_message() )
    {
        Manet::RouterMessage routerRequest;
        routerRequest.ParseFromString(requestMessage.message());

        if(requestMessage.header().messagetype() == Manet::ROUTER)
        {
            responseMessageHeader->set_messagetype(Manet::ROUTER);
            Manet::RouterMessage routerResponse;

            if(_smfServicePtr->SetRouterItem(routerRequest, routerResponse))
            {
                responseMessageHeader->set_responsecode(Manet::SUCCESSFUL);
                responseMessage.set_message(routerResponse.SerializeAsString());
            }
            else
            {
                responseMessageHeader->set_responsecode(Manet::BAD_OPTION);
                responseMessage.set_message("Could not SET Router Item");
            }
        }
    }
    else
    {
        responseMessageHeader->set_responsecode(Manet::BAD_MESSAGE);
        responseMessage.set_message("Could not SET Router Item");
    }

    _response = responseMessage.SerializeAsString();
    _responseLength = responseMessage.ByteSize();
}

void SmfRouterController::HandleDeleteRequest(Manet::SmfMessage requestMessage)
{

}
