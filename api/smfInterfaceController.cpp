//==============================================================================
//
// AUTHORIZATION TO USE AND DISTRIBUTE
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that:
//
// (1) source code distributions retain this paragraph in its entirety,
//
// (2) distributions including binary code include this paragraph in its
//     entirety in the documentation or other materials provided with the
//     distribution, and
//
// (3) all advertising materials mentioning features or use of this software
//     display the following acknowledgment:
//
//         "This product includes software written and developed by the QUEST
//         Project Team of the Space and Terrestrial Communications Directorate
//         within U.S. Army Communications and Electronics Research Development
//         and Engineering Center (CERDEC)."
//
// The name of CERDEC, the name(s) of CERDEC employee(s), or any entity of the
// United States Government may not be used to endorse or promote products
// derived from this software, nor does the inclusion of the CERDEC written and
// developed software directly or indirectly suggest CERDEC or United States
// Government endorsement of this product.
//
// This software was developed under the guidance of James Nguyen, U.S. Army
// CERDEC, Space and Terrestrial Communications Directorate.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//==============================================================================

#include "smfInterfaceController.h"
#include "smfService.h"

SmfInterfaceController::SmfInterfaceController(SmfService* smfServicePtr, Manet::SmfMessageType type)
    : SmfController(smfServicePtr)
{
    _type = type;
}

void SmfInterfaceController::HandleGetRequest(Manet::SmfMessage requestMessage)
{
    Manet::SmfMessage responseMessage;
    Manet::SmfMessageHeader *responseMessageHeader = responseMessage.mutable_header();

    if( requestMessage.has_message() )
    {
        Manet::InterfaceMessage interfaceMessage;
        interfaceMessage.ParseFromString(requestMessage.message());

        if(requestMessage.header().messagetype() == Manet::INTERFACE)
        {
            responseMessageHeader->set_messagetype(Manet::INTERFACE);

            if(_smfServicePtr->GetInterfaceItem(interfaceMessage))
            {
                responseMessageHeader->set_responsecode(Manet::SUCCESSFUL);
                responseMessage.set_message(interfaceMessage.SerializeAsString());
            }
            else
            {
                responseMessageHeader->set_responsecode(Manet::BAD_OPTION);
                responseMessage.set_message("Could not GET Interface Item");
            }
        }
    }
    else
    {
        responseMessageHeader->set_responsecode(Manet::BAD_MESSAGE);
        responseMessage.set_message("Could not GET Interface Item");
    }

    _response = responseMessage.SerializeAsString();
    _responseLength = responseMessage.ByteSize();
}

void SmfInterfaceController::HandleGetNextRequest(Manet::SmfMessage requestMessage)
{

}

void SmfInterfaceController::HandlePutRequest(Manet::SmfMessage requestMessage)
{

}

void SmfInterfaceController::HandlePostRequest(Manet::SmfMessage requestMessage)
{

}

void SmfInterfaceController::HandleDeleteRequest(Manet::SmfMessage requestMessage)
{

}
