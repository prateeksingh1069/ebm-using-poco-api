// NSMPR.cpp
//
#include "NSMPR.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <queue>
#include "../client/NhdpProtoClient.h"

NSMPR::NSMPR()
{
    std::vector<Router> mpr;
    std::vector<Router> MPR;
    std::vector<Router> N1, N2;
    std::vector< std::vector<Router> > N1z;
    std::vector< std::vector<Router> > N2y;
    
    std::vector<Router> temp;
    std::vector<Router> temp1;
    std::vector<Router> temp2;
}

NSMPR::~NSMPR()
{
    
}

std::vector<Router> NSMPR::run() {
    //TODO: move N1, N2 population here
    std::cout << "S-MPR starting" << std::endl;
    int count = 0;
    // TODO: 1. initialize the set MPR to empty

    // TODO: 2. initialize the set N1 to include all 1-hop neighbors of n0

    // TODO: 3. initialize the set N2 to include all 2-hop neighbors, excluding n0 and any routers in N1
    //          nodes only reachable through N1 routers with NEVER priority are also excluded

    // 4. for each interface y in N1
    std::cout << "for each interface y in N1" << std::endl;
    for(unsigned int y=0; y < N1.size(); y++)
    {
        std::cout << "y: " << N1[y].address << std::endl;
        
        
        //initialize N2(y) to include members of N2 that are 1-hop neighbors of y
        std::vector<std::string> one_hop_neighbors = nhdp.getOneHopNeighbors(N1[y].address);
        for(unsigned int n = 0; n < one_hop_neighbors.size(); n++)
        {
            std::cout << "\t1-hop: " << one_hop_neighbors[n] << std::endl; 
            // check for match in N2
            for(unsigned int y2=0; y2 < N2.size(); y2++)
            {
                std::cout << "\t\tn2: " << N2[y2].address << std::endl;
                if (one_hop_neighbors[n].compare(N2[y2].address) == 0)
                {
                    std::cout << "\t\t\tmatch" << std::endl;
                    temp.push_back(N2[y2]);
                    count++;
                    std::cout << "21 /n" << std::endl;
                    std::cout << "22 /n" << std::endl;
                    addN2y(temp);
                }
            }
        }
        //if(count !=0)
        //{
        //std::cout << "22 /n" << std::endl;
        //addN2y(temp);
        //}
    }

        if(N2.size() == 0)
        {
            for(unsigned int y_1=0; y_1 < N1.size(); y_1++)
            {
                std::cout << "31 /n" << std::endl;
                addN2y(temp);
            }
        }

std::cout << "1 /n" << std::endl;

    // 5. for each interface x in N1
    for(unsigned int x = 0; x < N1.size(); x++)
    {
        //with a priority value of ALWAYS
        if (N1[x].priority == ALWAYS)
        {
std::cout << "2 /n" << std::endl;
            //select x as an MPR

            // 5A. add x to MPR and remove x from N1
            addMPR(N1[x]);
std::cout << "23 /n" << std::endl;
            //N1.erase(std::remove(N1.begin(), N1.end(), *x), N1.end());
            N1.erase(N1.begin() + x); //TODO: test
std::cout << "24 /n" << std::endl;

std::cout << "27 /n" << std::endl;
std::cout << "28 /n" << std::endl;
std::cout << "29 /n" << std::endl;
            // 5B. for each interface z in N2(x) remove z from N2
            for(unsigned int z = 0; z < N2y[x].size(); z++)
            {
std::cout << "30 /n" << std::endl;
                for(unsigned int z2 = 0; z2 < N2.size(); z2++)
                {
std::cout << "25 /n" << std::endl;
                    if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                    {
std::cout << "26 /n" << std::endl;
                        N2.erase(N2.begin() + z2); //TODO: test
std::cout << "3 /n" << std::endl;
                    }
                }
            }

            // 5C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
            for(unsigned int y=0; y < N1.size(); y++)
            {
                if(y != x)
                {
std::cout << "4 /n" << std::endl;
                    for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                    {
                        for(unsigned int x2 = 0; x2 < N2y[x].size(); x2++)
                        {
                            if((N2y[y][y2].address).compare(N2y[x][x2].address) == 0)
                            {
                                N2y[y].erase(N2y[y].begin() + y2); //TODO: test
std::cout << "5 /n" << std::endl;
                            }
                        }
                    }
                }
            }
        }
    }

    // 6. for each interface z in N2
    for(unsigned int z=0; z < N2.size(); z++)
    {
        //std::vector<Router> temp1;
        // initialize N1(z) to include members of N1 that are 1-hop neighbors of z
        std::vector<std::string> one_hop_neighbors = nhdp.getOneHopNeighbors(N2[z].address);
        for(unsigned int n = 0; n < one_hop_neighbors.size(); n++)
        {
            // check for match in N1
            for(unsigned int z2=0; z2 < N1.size(); z2++)
            {
                if (one_hop_neighbors[n].compare(N1[z2].address) == 0)
                {
std::cout << "6 /n" << std::endl;
                    temp1.push_back(N1[z2]);
std::cout << "7 /n" << std::endl;
                }
            }
        }
std::cout << "8 /n" << std::endl;
        addN1z(temp1);
std::cout << "9 /n" << std::endl;
    }

    // 7. for each interface x in N2
    for(unsigned int x=0; x < N2.size(); x++)
    {
std::cout << "32 /n" << std::endl;
std::cout << "N1z.size(): " << N1z.size() << std::endl;
std::cout << "N1z[0].size(): " << N1z[0].size() << std::endl;

    if(x < N1z.size())
    {

        // where N1(x) has only one member
        if(N1z[x].size() == 1)
        {
std::cout << "33 /n" << std::endl;
            //select x as an MPR

            // 7A. add x to MPR and remove x from N1
            addMPR(N2[x]);
std::cout << "10 /n" << std::endl;
            for(unsigned int x2 = 0; x2 < N1.size(); x2++)
            {
std::cout << "11 /n" << std::endl;
                if ((N1[x2].address).compare(N2[x].address) == 0)
                {
std::cout << "12 /n" << std::endl;
                    N1.erase(N1.begin() + x2); //TODO: test
std::cout << "13 /n" << std::endl;
                }
            }

            // 7B. for each interface z in N2(x) remove z from N2 and delete N1(z)
            for(unsigned int z = 0; z < N2y[x].size(); z++)
            {
                for(unsigned int z2 = 0; z2 < N2.size(); z2++)
                {
                    // remove z from N2
                    if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                    {
std::cout << "14 /n" << std::endl;
                        N2.erase(N2.begin() + z2); //TODO: test
std::cout << "15 /n" << std::endl;
                        // delete N1(z)
                        //std::vector<Router> temp2;
std::cout << "16 /n" << std::endl;
                        N1z[z2] = temp2;
std::cout << "17 /n" << std::endl;
                    }
                }
            }

            // 7C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
            for(unsigned int y=0; y < N1.size(); y++)
            {
                if((N1[y].address).compare(N2[x].address) != 0)
                {
                    for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                    {
                        for(unsigned int x2 = 0; x2 < N1.size(); x2++)
                        {
                            if ((N2[x].address).compare(N1[x2].address) == 0)
                            {
                                for(unsigned int x3 = 0; x3 < N2y[x2].size(); x3++)
                                {
                                    if((N2y[y][y2].address).compare(N2y[x2][x3].address) == 0)
                                    {
std::cout << "17 /n" << std::endl;
                                        N2y[y].erase(N2y[y].begin() + y2); //TODO: test
std::cout << "18 /n" << std::endl;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    }

std::cout << "34 /n" << std::endl;

    // 8. while N2 is not empty
    while(N2.size() > 0)
    {

std::cout << "35 /n" << std::endl;

        // select the interface x in N1 with the largest router priority that has members in N2(x)
        unsigned int x = 0;

    if(x < N1.size())
    {
        for (unsigned int x2 = 0; x2 < N1.size(); x2++)
        {
std::cout << "44 /n" << std::endl;
            if(N1[x2].priority > N1[x].priority)
            {
std::cout << "45 /n" << std::endl;
                x = x2;
            }
        }

std::cout << "46 /n" << std::endl;

std::cout << "N1.size(): " << N1.size() << std::endl;
//std::cout << "N1[0].size(): " << N1[0].size() << std::endl;

        //select x as an MPR

        // 5A. add x to MPR and remove x from N1
        addMPR(N1[x]);

std::cout << "47 /n" << std::endl;


        //N1.erase(std::remove(N1.begin(), N1.end(), *x), N1.end());
        N1.erase(N1.begin() + x); //TODO: test

       
    // 5B. for each interface z in N2(x) remove z from N2

std::cout << "36 /n" << std::endl;

    }

    if(x < N2y.size())
    {

        for(unsigned int z = 0; z < N2y[x].size(); z++)
        {
std::cout << "37 /n" << std::endl;
            for(unsigned int z2 = 0; z2 < N2.size(); z2++)
            {
                if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                {
                    N2.erase(N2.begin() + z2); //TODO: test
                }
            }
        }

std::cout << "38 /n" << std::endl;

    }

        // 5C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
        for(unsigned int y=0; y < N1.size(); y++)
        {
            if(y != x)
            {
std::cout << "39 /n" << std::endl;
                for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                {
std::cout << "40 /n" << std::endl;
                    for(unsigned int x2 = 0; x2 < N2y[x].size(); x2++)
                    {
std::cout << "41 /n" << std::endl;
                        if((N2y[y][y2].address).compare(N2y[x][x2].address) == 0)
                        {
std::cout << "42 /n" << std::endl;
                            N2y[y].erase(N2y[y].begin() + y2); //TODO: test
std::cout << "43 /n" << std::endl;
                        }
                    }
                }
            }
        }

        if(N2.size() > 0)
        {
            break;
        }

    }

    return MPR;
}

void NSMPR::setn0(Router n) {
    n0 = n;
}

void NSMPR::setn0(int priority, std::string id, std::string address) {
    n0 = {priority, id, address, true};
}

void NSMPR::setMPR(std::vector<Router> mpr) {
    MPR = mpr;
}

void NSMPR::addMPR(Router n) {
    MPR.push_back(n);
}

void NSMPR::addMPR(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    MPR.push_back(n);
}

void NSMPR::setN1(std::vector<Router> n1) {
    N1 = n1;
}

void NSMPR::addN1(Router n) {
    N1.push_back(n);
}

void NSMPR::addN1(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N1.push_back(n);
}

void NSMPR::addN1z(std::vector<Router> n) {
    N1z.push_back(n);
}

void NSMPR::addN1z(Router n, int z) {
    N1z[z].push_back(n);
}

void NSMPR::addN1z(int priority, std::string id, std::string address, int z) {
    Router n = {priority, id, address, false};
    N1z[z].push_back(n);
}

void NSMPR::setN2(std::vector<Router> n2) {
    N2 = n2;
}

void NSMPR::addN2(Router n) {
    N2.push_back(n);
}

void NSMPR::addN2(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N2.push_back(n);
}

void NSMPR::addN2y(std::vector<Router> n) {
    N2y.push_back(n);
}

void NSMPR::addN2y(Router n, int y) {
    N2y[y].push_back(n);
}

void NSMPR::addN2y(int priority, std::string id, std::string address, int y) {
    Router n = {priority, id, address, false};
    N2y[y].push_back(n);
}

