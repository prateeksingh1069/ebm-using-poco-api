
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdint>
#include <ctype.h>
#include "client/NhdpProtoClient.h"
#include "algo/ECDS.h"
#include "algo/SMPR.h"
#include "algo/NSMPR.h"
#include "client/EbmProtoClient.h"
#include "server/EbmServer.h"
#include "client/SmfProtoClient.h"


//bool CalcRelaySelectionAlgo(std::string a, std::string b);
bool CalcRelaySelectionAlgoCF(std::string a);
bool CalcRelaySelectionAlgoECDS(std::string a);
std::vector<Router> CalcRelaySelectionAlgoSMPR(std::string a);
std::vector<Router> CalcRelaySelectionAlgoN_SMPR(std::string a);
std::vector<std::string> MPR_selector_set_str;
std::vector<std::string> Neighbour_selector_set_str;
std::vector<Router> MPR_set[20];
std::vector<Router> mpr_set;
bool selected;
bool isRelay;
char** findAllIpAddr();
char* findMacAddr(std::string selector_host);
char* mpr_mac_Addr;
const char* MPR_selector_set;
const char* Neighbour_selector_set;
static char* hex_to_ip(const char *input);
char** allNodeIp;
std::string host_other;
int count=0;
std::vector<std::string> one_hop_neighbors;
std::vector<std::string> one_hop_neighbors1;
std::vector<std::string> two_hop_neighbors;
char MAC[256];
std::string s;

int main(int argc, char *argv[])
{

	if (argc != 3) {
	        std::cout << "Usage: host algo{cf,ecds,smpr,nsmpr}" << std::endl;
	        exit(1);
    }
    std::string host = argv[1];
    std::string algo = argv[2];



////////////////////////////////////////////////////////////////////////Running relay_selection algorithm and sending the result to SMF using API implementation



    SmfProtoClient smf_test_client;

    if(algo == "cf"){
    	isRelay = CalcRelaySelectionAlgoCF(host);
    	smf_test_client.SendToSmf(host, isRelay);
    }
    else if(algo == "ecds"){
    	isRelay = CalcRelaySelectionAlgoECDS(host);
    	smf_test_client.SendToSmf(host, isRelay);
    }
    else if(algo == "smpr"){
    	allNodeIp = findAllIpAddr();
    	//MPR_set[0] = CalcRelaySelectionAlgoSMPR(host);
    	printf("count : %d \n" , count);
    	for(int i=0;i<count;i++)
    	{

    		host_other = std::string(allNodeIp[i]);

    		MPR_set[i] = CalcRelaySelectionAlgoSMPR(host_other);

    		std::vector<Router> mpr_set_storage = MPR_set[i];

            std::cout << "MPR_set[i] with i+1: " << i << " :    "<< std::endl;
		    for (unsigned int j = 0; j < mpr_set_storage.size(); j++)
		    {

		       std::string selector_host = mpr_set_storage[j].address;

		       std::cout << mpr_set_storage[j].address << " "<< std::endl;
		       if(selector_host == host)
		       {

		    	   mpr_mac_Addr = findMacAddr(host_other);

		    	   MPR_selector_set_str.push_back(mpr_mac_Addr) ;

		       }
		    }


    	}
    	//smf_test_client.SendToSmf(host, isRelay); // TODO

        std::cout << "MPR_selector_set_str: ";
        for (unsigned int i = 0; i < MPR_selector_set_str.size(); i++)
        {
            std::cout << MPR_selector_set_str[i] << " ";
        }
        std::cout << std::endl;

	    for( std::vector<std::string>::const_iterator i = MPR_selector_set_str.begin(); i != MPR_selector_set_str.end(); ++i)
	    {
	    	s += *i;
	    }

        MPR_selector_set = s.c_str();

        std::cout << "FINAL DATA TO BE SENT, MPR_selector_set !!!!!!!!!: " << MPR_selector_set << std::endl;

        smf_test_client.SendToSmf(host, MPR_selector_set);

        std::cout << "FINAL DATA MPR_selector_set being sent to SmfProtoClient: " << std::endl;

        //////////////////////////Neighbour Selector List
        NhdpProtoClient test_client1;
        one_hop_neighbors1 = test_client1.getOneHopNeighbors(host);

        for (unsigned int j = 0; j < one_hop_neighbors1.size(); j++)
		    {

		    	   mpr_mac_Addr = findMacAddr(one_hop_neighbors1[j]);

		    	   Neighbour_selector_set_str.push_back(mpr_mac_Addr) ;

		    }

        for( std::vector<std::string>::const_iterator i = Neighbour_selector_set_str.begin(); i != Neighbour_selector_set_str.end(); ++i)
	    {
	    	s += *i;
	    }
		    

        Neighbour_selector_set = s.c_str();

        smf_test_client.SendToSmf_NgbrList(host, Neighbour_selector_set);

        std::cout << "Neighbour_selector_set being sent to SmfProtoClient: " << std::endl;

    }
    else if(algo == "nsmpr"){
    	allNodeIp = findAllIpAddr();
    	printf("count : %d \n" , count);
    	for(int i=0;i<count;i++)
    	{

    		host_other = std::string(allNodeIp[i]);

    		MPR_set[i] = CalcRelaySelectionAlgoN_SMPR(host_other);

    		std::vector<Router> mpr_set_storage = MPR_set[i];

            std::cout << "MPR_set[i] with i+1: " << i << " :    "<< std::endl;
		    for (unsigned int j = 0; j < mpr_set_storage.size(); j++)
		    {

		       std::string selector_host = mpr_set_storage[j].address;

		       std::cout << mpr_set_storage[j].address << " "<< std::endl;
		       if(selector_host == host)
		       {

		    	   mpr_mac_Addr = findMacAddr(host_other);

		    	   MPR_selector_set_str.push_back(mpr_mac_Addr) ;

		       }
		    }


    	}
    	

        std::cout << "MPR_selector_set_str: ";
        for (unsigned int i = 0; i < MPR_selector_set_str.size(); i++)
        {
            std::cout << MPR_selector_set_str[i] << " ";
        }
        std::cout << std::endl;

	    for( std::vector<std::string>::const_iterator i = MPR_selector_set_str.begin(); i != MPR_selector_set_str.end(); ++i)
	    {
	    	s += *i;
	    }

        MPR_selector_set = s.c_str();

        std::cout << "NS-MPR FINAL DATA TO BE SENT, MPR_selector_set !!!!!!!!!: " << MPR_selector_set << std::endl;

        smf_test_client.SendToSmfN_SMPR(host, MPR_selector_set);

    }
    else{
        std::cout << "unknown algorithm: " << algo << std::endl;
    }



////////////////////////////////////////////////////////////////////////


     //EBM server
    std::cout << "Listening for EBM messages" << std::endl;
    EbmServer serv(5514);
    serv.run();

    return 0;

} // end of main()


bool CalcRelaySelectionAlgoCF(std::string host)
{
	selected = true;
	return selected;
}

bool CalcRelaySelectionAlgoECDS(std::string host)
{
	std::cout << "GETTING NHDP NEIGHBORS FROM " << host << std::endl;
	    NhdpProtoClient test_client;

	    //get one-hop neighbors
	    one_hop_neighbors = test_client.getOneHopNeighbors(host);
	    std::cout << "1-hop: ";
	    for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
	    {
	        std::cout << *i << ' ';
	    }
	    std::cout << std::endl;

	    //get two-hop neighbors
	    two_hop_neighbors = test_client.getTwoHopNeighbors(host);
	    std::cout << "2-hop: ";
	    for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
	    {
	        std::cout << *i << ' ';
	    }
	    std::cout << std::endl;


	    //E-CDS

	        std::cout << "RUNNING E-CDS" << std::endl;
	        ECDS test_ECDS;
	        //TODO: get router priority
	        int priority = 1;

	        //set n0
	        std::cout << "setting n0: " << host << std::endl;
	        test_ECDS.setn0(priority, host, host);

	        //initialize N1 with 1-hop neighbors of n0
	        for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
	        {
	            std::cout << "adding to N1: " << *i << std::endl;
	            test_ECDS.addN1(priority, *i, *i);
	        }
	        std::cout << std::endl;

	        //initialize N2 with 2-hop neighbors of n0
	        for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
	        {
	            std::cout << "adding to N2: " << *i << std::endl;
	            test_ECDS.addN2(priority, *i, *i);
	        }
	        std::cout << std::endl;

	        //run algorithm
	        std::cout << "running..." << std::endl;
	        selected = test_ECDS.run(); //TODO: .run(host)
	        std::cout << "n0 selected: " << selected << std::endl;
	        return selected;

}

std::vector<Router> CalcRelaySelectionAlgoSMPR(std::string host)   // SMPR Algorithm
{
	std::cout << "GETTING NHDP NEIGHBORS FROM " << host << std::endl;
		    NhdpProtoClient test_client;

		    //get one-hop neighbors
		    one_hop_neighbors = test_client.getOneHopNeighbors(host);
		    std::cout << "1-hop: ";
		    for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
		    {
		        std::cout << *i << ' ';
		    }
		    std::cout << std::endl;

		    //get two-hop neighbors
		    two_hop_neighbors = test_client.getTwoHopNeighbors(host);
		    std::cout << "2-hop: ";
		    for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
		    {
		        std::cout << *i << ' ';
		    }
		    std::cout << std::endl;

		    //S-MPR ; To be done

		            std::cout << "RUNNING S-MPR" << std::endl;
		            SMPR test_SMPR;
		            //TODO: get router priority
		            int priority = 1;

		            //set n0
		            std::cout << "setting n0: " << host << std::endl;
		            test_SMPR.setn0(priority, host, host);

		            //initialize MPR to empty

		            //initialize N1 with 1-hop neighbors of n0
		            for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
		            {
		                std::cout << "adding to N1: " << *i << std::endl;
		                test_SMPR.addN1(priority, *i, *i);
		            }
		            std::cout << std::endl;

		            //initialize N2 with 2-hop neighbors of n0
		            for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
		            {
		                //std::cout << "[" << *i << "]" << std::endl;
		                if((*i).length() > 0)
		                {
		                    //TODO: fix for removing prefix neighbor (e.g. "10.0.0.210.0.0.3")
		                    std::string i_fix = *i;
		                    //i_fix = i_fix.erase(0, 8); // remove first ip
		                    i_fix = i_fix.erase(8, i_fix.length()); // remove second ip

		                    //TODO: fix
		                    if(i_fix.length() > 0)
		                    {
		                        //exclude any routers in N1
		                        bool in_N1 = false;
		                        for( std::vector<std::string>::const_iterator i2 = one_hop_neighbors.begin(); i2 != one_hop_neighbors.end(); ++i2)
		                        {
		                            if (i_fix.compare(*i2) == 0) {
		                                in_N1 = true;
		                            }
		                        }
		                        if(!in_N1) {
		                            std::cout << "adding to N2: " << i_fix << "" << std::endl;
		                            test_SMPR.addN2(priority, i_fix, i_fix);
		                        }
		                    }
		                }
		            }
		            std::cout << std::endl;

		            //run algorithm
		            std::cout << "running..." << std::endl;
		            mpr_set = test_SMPR.run(); //TODO: .run(host)
		            std::cout << "mpr_set: ";
		            for (unsigned int i = 0; i < mpr_set.size(); i++)
		            {
		                std::cout << mpr_set[i].address << " ";
		            }
		            std::cout << std::endl;

		            return mpr_set;
}


std::vector<Router> CalcRelaySelectionAlgoN_SMPR(std::string host)  // NSMPR Algorithm
{
	std::cout << "GETTING NHDP NEIGHBORS for NSMPR " << host << std::endl;
		    NhdpProtoClient test_client;

		    //get one-hop neighbors
		    one_hop_neighbors = test_client.getOneHopNeighbors(host);
		    std::cout << "1-hop: ";
		    for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
		    {
		        std::cout << *i << ' ';
		    }
		    std::cout << std::endl;

		    //get two-hop neighbors
		    two_hop_neighbors = test_client.getTwoHopNeighbors(host);
		    std::cout << "2-hop: ";
		    for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
		    {
		        std::cout << *i << ' ';
		    }
		    std::cout << std::endl;

		    //S-MPR ; To be done

		            std::cout << "RUNNING NS-MPR" << std::endl;
		            NSMPR test_NSMPR;
		            //TODO: get router priority
		            int priority = 1;

		            //set n0
		            std::cout << "setting n0: " << host << std::endl;
		            test_NSMPR.setn0(priority, host, host);

		            //initialize MPR to empty

		            //initialize N1 with 1-hop neighbors of n0
		            for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
		            {
		                std::cout << "adding to N1: " << *i << std::endl;
		                test_NSMPR.addN1(priority, *i, *i);
		            }
		            std::cout << std::endl;

		            //initialize N2 with 2-hop neighbors of n0
		            for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
		            {
		                //std::cout << "[" << *i << "]" << std::endl;
		                if((*i).length() > 0)
		                {
		                    //TODO: fix for removing prefix neighbor (e.g. "10.0.0.210.0.0.3")
		                    std::string i_fix = *i;
		                    //i_fix = i_fix.erase(0, 8); // remove first ip
		                    i_fix = i_fix.erase(8, i_fix.length()); // remove second ip

		                    //TODO: fix
		                    if(i_fix.length() > 0)
		                    {
		                        //exclude any routers in N1
		                        bool in_N1 = false;
		                        for( std::vector<std::string>::const_iterator i2 = one_hop_neighbors.begin(); i2 != one_hop_neighbors.end(); ++i2)
		                        {
		                            if (i_fix.compare(*i2) == 0) {
		                                in_N1 = true;
		                            }
		                        }
		                        if(!in_N1) {
		                            std::cout << "adding to N2: " << i_fix << "" << std::endl;
		                            test_NSMPR.addN2(priority, i_fix, i_fix);
		                        }
		                    }
		                }
		            }
		            std::cout << std::endl;

		            //run algorithm
		            std::cout << "running NSMPR..." << std::endl;
		            mpr_set = test_NSMPR.run(); //TODO: .run(host) for NSMPR
		            std::cout << "mpr_set: ";
		            for (unsigned int i = 0; i < mpr_set.size(); i++)
		            {
		                std::cout << mpr_set[i].address << " ";
		            }
		            std::cout << std::endl;

		            return mpr_set;
}


char** findAllIpAddr()
{
	FILE *f;
	    char line[100] , *p , *c;
	    char** ip;
	    int i=0;
	    //int count = 0;
        //const char* host_ip = "0000000A";
	    //host_ip = host.c_str();

	    //f = fopen("/home/quest/Documents/EBM/route" , "r");
        f = fopen("/proc/net/route" , "r");

	    while(fgets(line , 100 , f))
	    {
	        c = strtok(line , "	");
	        p = strtok(NULL , " \t");
	        //printf("c is : %s \n" , c);
	        //printf("p is : %s \n" , p);


	        if(p!=NULL && c!=NULL)
	        {
	            if(strcmp(c , "eth0") == 0)
	            {
                    if(strcmp(p , "0000000A") == 0)
                    {
                    }
                    else
                    {
                        //std::cout << "0 "<< std::endl;
	                    printf("Ip Addresses is : %s \n" , p);
	                    ip[i] = hex_to_ip(p);
                        //std::cout << "1 "<< std::endl;
	                    printf("Ip Addresses in decimal form is : %s \n" , ip[i]);
                        //std::cout << "4 "<< std::endl;
	                    i++;
	                    count++;

	                    //break;
                    }
	            }
	        }


	    }
	    return ip;
}

char* findMacAddr(std::string host_other)
{
	const int size = 256;
		char ip_address[size];
		char mac_address[size];
		//char MAC[size];
	    int hw_type;
	    int flags;
	    //const char* host_other = "10.0.0.1";
	    //int i=0;
	    //char mac_address[size];
	    //char* mac_address;
	    char mask[size];
	    char device[size];

	    const char* c;
	    c = host_other.c_str();
	    FILE* fp = fopen("/home/quest/Documents/EBM/macAddress", "r");
	    if(fp == NULL)
	    {
	        perror("Error opening /proc/net/arp");
	    }

	    char line[size];
	    fgets(line, size, fp);    // Skip the first line, which consists of column headers.
	    while(fgets(line, size, fp))
	    {
	        sscanf(line, "%s 0x%x 0x%x %s %s %s\n",
	               ip_address,
	               &hw_type,
	               &flags,
	               mac_address,
	               mask,
	               device);

	        printf("IP = %s, MAC = %s \n", ip_address, mac_address);
	        printf("host_other*[size] = %s \n", c);
	        if(!strcmp(ip_address, c))
	        {
	        	//MAC = mac_address;
	        	strcpy(MAC, mac_address);
	        	printf("MATCHED!!!! IP = %s, MAC = %s \n", ip_address, MAC);

	        }
	        //i++;
	    }


	    fclose(fp);
	    return MAC;
}

static char* hex_to_ip(const char *input)
{
    //std::cout << "2 "<< std::endl;
    char *output = (char*)malloc(sizeof(char) * 16);
    //std::cout << "3 "<< std::endl;
    unsigned int a, b, c, d;

    if (sscanf(input, "%2x%2x%2x%2x", &a, &b, &c, &d) != 4)
        return output;
    sprintf(output, "%u.%u.%u.%u", d, c, b, a);
    return output;
    //std::cout << "4 "<< std::endl;
}

