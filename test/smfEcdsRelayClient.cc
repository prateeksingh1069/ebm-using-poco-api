#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "../proto/manet.pb.h"
//#include "../../../Downloads/cerdec-manet/smf/protobuf/manet.pb.h"
#include "../proto/smf.pb.h"

//#include "protoAddress.h"
//#include "protoSocket.h"

#include "../protolib/include/protoAddress.h"
#include "../protolib/include/protoSocket.h"


//using protolib::include::ProtoAddress;
//using protolib::include::ProtoSocket;
//ProtoAddress address;
//ProtoSocket socket;

int main(int argc, char** argv)
{
	if (argc != 3) {
        std::cout << "Usage: host isRelay{true,false}" << std::endl;
        exit(1);
    }
    ProtoAddress address;
    address.ResolveFromString(argv[1]);
    address.SetPort(5557);

    ProtoSocket socket(ProtoSocket::UDP);

    Manet::RouterMessage router;
    router.set_id("");

    if(!strcmp(argv[2],"true"))
        router.SetExtension(Manet::SmfRouterMessage::isRelay,true);
    else if(!strcmp(argv[2],"false"))
        router.SetExtension(Manet::SmfRouterMessage::isRelay,false);
    else
    {
        std::cout << "invalid isRelay command" << std::endl;
        return 1;
    }

    Manet::SmfMessage request;
    Manet::SmfMessageHeader *header = request.mutable_header();
    header->set_messagetype(Manet::ROUTER_RELAY_ECDS);
    header->set_optype(Manet::POST);
    request.set_message(router.SerializeAsString());

    if(socket.SendTo(request.SerializeAsString().c_str(), request.ByteSize(), address))
    {
        std::cout << "successful send" << std::endl;
		char buf[8192];
        unsigned int size = 8192;
        if(socket.Recv(buf,size))
        {
			std::cout << "Received. " << std::endl;
			Manet::SmfMessage response;
            response.ParseFromArray(buf,size);
			std::cout << "Response Code:	" << Manet::ManetResponseCode_Name(response.header().responsecode()) << std::endl;
			if ( response.header().responsecode() == Manet::SUCCESSFUL )
			{
				Manet::RouterMessage routerResponse;
				routerResponse.ParseFromString(response.message());
				std::cout << "Is relay selected(1 or 0)? " << routerResponse.GetExtension(Manet::SmfRouterMessage::isRelay) << std::endl;
			}
			else
				std::cout << response.message() << std::endl;
		}
    }
    else
    {
        std::cout << "send to socket failed" << std::endl;
    }

    return 0;
}

